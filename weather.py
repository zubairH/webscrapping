import requests
from bs4 import BeautifulSoup

# Define the URL of the website and the location to fetch weather for
url = 'https://forecast.weather.gov/MapClick.php'
params = {
    'lat': 37.7772,
    'lon': -122.4168,
    'unit': '0',
    'lg': 'english',
    'FcstType': 'graph'
}

# Make a request to the website with the given location parameters
response = requests.get(url, params=params)

# Parse the HTML content using BeautifulSoup
soup = BeautifulSoup(response.content, 'html.parser')

# Find the current temperature and weather condition
current_temp = soup.find('p', class_='myforecast-current-lrg').text
weather_condition = soup.find('p', class_='myforecast-current').text

# Print the results
print(f'Current temperature: {current_temp}')
print(f'Weather condition: {weather_condition}')
